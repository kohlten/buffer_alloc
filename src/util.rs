use core::marker::PhantomData;
use libc_print::libc_println;
use libc::exit;

use crate::{PAGE_HEAD, BlockHeader, get_block_header, get_page_header, PageHeader, PAGE_HEADER_SIZE, BLOCK_HEADER_SIZE, MAGIC};

/// Iterator for pages. Takes the page address, and iterates through the different pages. Returns the page header and the location of the page in memory.
pub(crate) struct PageIter<'a> {
    current: *mut u8,
    phantom: PhantomData<&'a u32>,
}

impl<'a> PageIter<'a> {
    pub fn new(ptr: *mut u8) -> Self {
        Self {
            current: ptr,
            phantom: Default::default()
        }
    }
}

impl<'a> Iterator for PageIter<'a> {
    type Item = (*mut u8, &'a mut PageHeader);

    fn next(&mut self) -> Option<Self::Item> {
        if self.current.is_null() {
            return None;
        }
        let header = unsafe { get_page_header(self.current) };
        let addr = self.current;
        if header.magic != MAGIC {
            libc_println!("Magic check failed. Corrupt data at {:?}!", self.current);
            unsafe { exit(-1) };
        }
        self.current = header.next_page;
        Some((addr, header))
    }
}

/// Iterator for blocks in a page. Will iterate through all the blocks on the page. Returns the block address and the block header.
pub(crate) struct BlockIter<'a> {
    current: *mut u8,
    phantom: PhantomData<&'a u32>,
}

impl<'a> BlockIter<'a> {
    pub fn new(ptr: *mut u8) -> Self {
        Self {
            current: ptr,
            phantom: Default::default()
        }
    }
}

impl<'a> Iterator for BlockIter<'a> {
    type Item = (*mut u8, &'a mut BlockHeader);

    fn next(&mut self) -> Option<Self::Item> {
        if self.current.is_null() {
            return None;
        }
        let header = unsafe { get_block_header(self.current) };
        let addr = self.current;
        if header.magic != MAGIC {
            libc_println!("Magic check failed. Corrupt data at {:?}!", self.current);
            unsafe { exit(-1) };
        }
        self.current = header.next_block;
        Some((addr, header))
    }
}

/// Stats of the currently allocated memory.
#[derive(Copy, Clone, Default, Debug)]
pub struct Stats {
    pub pages: usize,
    pub free_slots: usize,
    pub free_memory: usize,
    pub allocated_memory: usize,
    pub allocated_user_memory: usize,
    pub total_size: usize,
}

/// Returns the stats of the allocated memory.
pub fn get_stats() -> Stats {
    let mut stats = Stats::default();
    for (_, page) in PageIter::new(unsafe { PAGE_HEAD }) {
        stats.pages += 1;
        stats.total_size += PAGE_HEADER_SIZE;
        stats.allocated_memory += PAGE_HEADER_SIZE;
        for (_, block) in BlockIter::new(page.head) {
            if block.free {
                stats.free_slots += 1;
                stats.free_memory += block.size;
            } else {
                stats.allocated_memory += block.size + BLOCK_HEADER_SIZE;
                stats.allocated_user_memory += block.size;
            }
            stats.total_size += block.size;
            stats.allocated_memory += BLOCK_HEADER_SIZE;
        }
    }
    return stats;
}

/// Prints the current headers of the memory.
pub fn print_state() {
    let mut page_id = 0;
    for (page_address, page) in PageIter::new(unsafe { PAGE_HEAD }) {
        libc_println!("Page #: {}, Page Address: {:?}", page_id, page_address);
        for (block_address, block) in BlockIter::new(page.head) {
            libc_println!("Location: {:?}, Size: {}, Free: {}, Next: {:?}", block_address, block.size, block.free, block.next_block);
        }
        page_id += 1;
        libc_println!("________________________________");
    }
    let stats = get_stats();
    libc_println!("Free Slots: {}, Total Free Memory: {}, Total Allocated Memory: {}, User Allocated Memory: {}, Total Memory: {}", stats.free_slots, stats.free_memory, stats.allocated_memory, stats.allocated_user_memory, stats.total_size);
}