//! A simple memory allocator for rust. This library allows you do have a set buffer size that you can then allocate all of your memory from.
//!
//! The pointer returned to the user will be the beginning of the memory unit.
//!
//! If you wish to see the internals of what it's doing, use the feature flags 'debug' and 'verbose'.
//!
//! See examples/basic on how to use this library.

#![no_std]

pub mod util;

extern crate libc;

use core::alloc::{GlobalAlloc, Layout};
use core::mem::size_of;
use core::mem::transmute;
use core::ptr::{null_mut, slice_from_raw_parts};

use libc::{c_void, exit, free, malloc, memcpy, memset};
use libc_print::libc_println;

#[cfg(any(feature = "debug", feature = "verbose"))]
use crate::util::print_state;
use crate::util::{BlockIter, PageIter};

/// Header for the page. Contains the head of the memory for the page, and the next page if there is one.
struct PageHeader {
    magic: u16,
    head: *mut u8,
    next_page: *mut u8,
    prev_page: *mut u8,
}

impl PageHeader {
    pub fn new(head: *mut u8, next_page: *mut u8, prev_page: *mut u8) -> Self {
        Self {
            magic: MAGIC,
            head,
            next_page,
            prev_page,
        }
    }
}

/// Header for the block. Contains the size of the block, whether it's free for not, and the next block if there is one.
struct BlockHeader {
    magic: u16,
    size: usize,
    free: bool,
    next_block: *mut u8,
}

impl BlockHeader {
    pub fn new(size: usize, free: bool, next_block: *mut u8) -> Self {
        Self {
            magic: MAGIC,
            size,
            free,
            next_block,
        }
    }
}

const PAGE_HEADER_SIZE: usize = size_of::<PageHeader>();
const BLOCK_HEADER_SIZE: usize = size_of::<BlockHeader>();

const MAGIC: u16 = 0xd41d;

static mut PAGE_SIZE: usize = 67000000; // 67 MB

static mut PAGE_HEAD: *mut u8 = null_mut();

/// Gerts the header of the page. The address should be at the start of the block header.
unsafe fn get_page_header<'a>(page: *mut u8) -> &'a mut PageHeader {
    transmute::<*mut u8, &'a mut PageHeader>(page.offset(-(PAGE_HEADER_SIZE as isize)))
}

/// Gets the header of the block. The address should be at the start of the memory.
unsafe fn get_block_header<'a>(block: *mut u8) -> &'a mut BlockHeader {
    transmute::<*mut u8, &'a mut BlockHeader>(block.offset(-(BLOCK_HEADER_SIZE as isize)))
}

/// Splits a block into two blocks with new_size being the size of the first block. The rest of the memory will go to the second block.
unsafe fn split_block(block: *mut u8, new_size: usize) {
    let header = get_block_header(block);
    if header.size == new_size {
        header.free = false;
        return;
    }
    let previous_size = header.size;
    header.size = new_size;
    header.free = false;
    let new_block = BlockHeader::new(previous_size - new_size - BLOCK_HEADER_SIZE, true, header.next_block);
    let new_block_location = block.offset(new_size as isize);
    memcpy(new_block_location as *mut c_void, slice_from_raw_parts(&new_block, BLOCK_HEADER_SIZE) as *mut c_void, BLOCK_HEADER_SIZE);
    header.next_block = new_block_location.offset(BLOCK_HEADER_SIZE as isize);
}

/// Combines two consecutive blocks together. This makes their memory attached to one block instead of two. The second header is included in the memory.
unsafe fn combine_blocks(first_block: *mut u8) {
    let header = get_block_header(first_block);
    if header.next_block.is_null() {
        libc_println!("Tried to combine consecutive blocks together without a second block at {:?}", first_block);
        exit(1);
    }
    let next_header = get_block_header(header.next_block);
    header.size += next_header.size + BLOCK_HEADER_SIZE;
    header.next_block = next_header.next_block;
}

/// Creates a new page of memory with a size of 67108864 and then initializes the page with the page header and block header.
/// If there is no pages, it will assign the page to PAGE_HEAD, otherwise it will assign it to the last node in the page linked list.
unsafe fn new_page(mut size: usize) -> *mut u8 {
    #[cfg(feature = "debug")]
    libc_println!("New Page");
    if size >= PAGE_SIZE - (PAGE_HEADER_SIZE + BLOCK_HEADER_SIZE) {
        size += PAGE_HEADER_SIZE + BLOCK_HEADER_SIZE;
    } else {
        size = PAGE_SIZE;
    }
    let page_mem = malloc(size);
    if page_mem.is_null() {
        libc_println!("failed to allocate page of size {}", PAGE_SIZE);
        exit(1);
    }
    let block = BlockHeader::new(size - (PAGE_HEADER_SIZE + BLOCK_HEADER_SIZE), true, null_mut());
    memcpy(page_mem.offset(PAGE_HEADER_SIZE as isize), slice_from_raw_parts(&block, BLOCK_HEADER_SIZE) as *mut c_void, BLOCK_HEADER_SIZE);
    let mut page = PageHeader::new(page_mem.offset((PAGE_HEADER_SIZE + BLOCK_HEADER_SIZE) as isize) as *mut u8, null_mut(), null_mut());
    if PAGE_HEAD.is_null() {
        memcpy(page_mem, slice_from_raw_parts(&page, PAGE_HEADER_SIZE) as *mut c_void, PAGE_HEADER_SIZE);
        PAGE_HEAD = page_mem.offset(PAGE_HEADER_SIZE as isize) as *mut u8;
        return page.head;
    }
    let mut page_iter = PageIter::new(PAGE_HEAD);
    let mut last_header = page_iter.next().unwrap();
    for header in page_iter {
        last_header = header;
    }
    page.prev_page = last_header.0;
    memcpy(page_mem, slice_from_raw_parts(&page, PAGE_HEADER_SIZE) as *mut c_void, PAGE_HEADER_SIZE);
    last_header.1.next_page = page_mem.offset(PAGE_HEADER_SIZE as isize) as *mut u8;
    page.head
}

/// Goes through all the pages and sees if there is all free blocks. If there is, it will free that page.
/// The first page will never be deallocated until the end of the program to prevent malloc from being called again.
unsafe fn free_memory(page_address: *mut u8) {
    let page_header = get_page_header(page_address);
    if page_address == PAGE_HEAD {
        return;
    }
    let mut is_free = true;
    let block_header = get_block_header(page_header.head);
    if block_header.free && block_header.next_block.is_null() {
        is_free = false;
    }
    if is_free {
        if !page_header.prev_page.is_null() {
            let previous = get_page_header(page_header.prev_page);
            previous.next_page = page_header.next_page;
        }
        if !page_header.next_page.is_null() {
            let next = get_page_header(page_header.next_page);
            next.prev_page = page_header.prev_page;
        }
        #[cfg(feature = "debug")]
        libc_println!("Free Page -> Address: {:?}", page_address);
        #[cfg(feature = "verbose")]
            print_state();
        free(page_address.offset(-(PAGE_HEADER_SIZE as isize)) as *mut c_void);
    }
}

/// Allocates the internal memory bank and sets up the initial block.
unsafe fn init() {
    if !PAGE_HEAD.is_null() {
        return;
    }
    new_page(0);
    #[cfg(feature = "debug")]
    libc_println!("Page Size: {}, Block Size: {}, Start Loc: {:?}", PAGE_HEADER_SIZE, BLOCK_HEADER_SIZE, PAGE_HEAD);
}

/// Go though all the pages and the blocks of each page. If we have two blocks in continuous memory that are free, combine them into one block.
unsafe fn resize_blocks(page_address: *mut u8) {
    let page = get_page_header(page_address);
    loop {
        let mut changed = false;
        for (_, current_header) in BlockIter::new(page.head) {
            if current_header.next_block.is_null() {
                break;
            }
            let next_header = get_block_header(current_header.next_block);
            // Add the data into the first block, and assign the next of the current to the next next.
            if current_header.free && next_header.free {
                current_header.size += next_header.size + BLOCK_HEADER_SIZE;
                current_header.next_block = next_header.next_block;
                changed = true;
            }
        }
        if !changed {
            break;
        }
    }
}

#[derive(Clone, Debug, Default)]
pub struct BufferAlloc;

impl BufferAlloc {
    /// Allocates new memory and copies the previous memory to the new memory.
    #[inline]
    unsafe fn realloc_new(&self, ptr: *mut u8, layout: Layout, size: usize) -> *mut u8 {
        #[cfg(feature = "debug")]
        libc_println!("Reallocate_New -> Previous Size: {} Size: {},  Ptr: {:?}", layout.size(), size, ptr);
        let layout_size = layout.size();
        let mem = self.alloc(Layout::from_size_align(size, layout.align()).unwrap());
        if layout_size < size {
            memcpy(mem as *mut c_void, ptr as *mut c_void, layout.size());
        } else {
            memcpy(mem as *mut c_void, ptr as *mut c_void, size);
        }
        self.dealloc(ptr, layout);
        mem
    }
}

unsafe impl GlobalAlloc for BufferAlloc {
    /// Allocates new memory using the size of the layout.
    #[inline]
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        #[cfg(feature = "debug")]
        libc_println!("Allocate -> Size: {}", layout.size());
        init();
        let size = layout.size();
        for (_, page_header) in PageIter::new(PAGE_HEAD) {
            let mut best_loc = page_header.head;
            let mut best_diff = -1;
            for (block_address, block_header) in BlockIter::new(page_header.head) {
                if block_header.free {
                    let diff = block_header.size as i32 - size as i32;
                    if diff == 0 { // If we allocated this page for this allocation or the memory so happens to be exactly the right size, use this slot.
                        best_diff = diff;
                        best_loc = block_address;
                    } else if diff >= 0 && (diff < best_diff || best_diff == -1) && diff >= BLOCK_HEADER_SIZE as i32 {
                        best_diff = diff;
                        best_loc = block_address;
                    }
                }
            }
            if best_diff != -1 {
                if best_diff != 0 { // If the block can be split large enough for a header, split the block into two.
                    split_block(best_loc, size);
                } else { // If the block is exactly the right size, unfree the block and return it.
                    let header = get_block_header(best_loc);
                    header.free = false;
                }
                #[cfg(feature = "verbose")]
                    print_state();
                return best_loc;
            }
        }
        // If we don't have enough room on the pages, make a new page for this allocation.
        new_page(layout.size());
        self.alloc(layout)
    }

    /// Deallocates the memory located at ptr.
    #[inline]
    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        // We assume we are inited here because a dealloc should never be called if we haven't allocated before.
        #[cfg(feature = "debug")]
        libc_println!("Deallocate -> Size: {}, Ptr: {:?}", layout.size(), ptr);
        let size = layout.size();
        let mut selected_block = None;
        let mut selected_page = null_mut();
        for (page_address, page) in PageIter::new(PAGE_HEAD) {
            for (current, block) in BlockIter::new(page.head) {
                if current == ptr {
                    selected_page = page_address;
                    selected_block = Some(block);
                    break;
                }
            }
            if selected_block.is_some() {
                break;
            }
        }
        if selected_block.is_none() {
            #[cfg(feature = "debug")]
            libc_println!("Tried to free ptr at {:?} that was not allocated by us", ptr);
            return;
        }
        if size != selected_block.as_ref().unwrap().size {
            libc_println!("Data at {:?} had size {}, expected {}", ptr, size, selected_block.as_ref().unwrap().size);
            exit(1);
        }
        selected_block.unwrap().free = true;
        resize_blocks(selected_page);
        free_memory(selected_page);
        #[cfg(feature = "verbose")]
            print_state();
    }

    /// Allocate new memory that is zeroed. See [self.alloc] for more details.
    #[inline]
    unsafe fn alloc_zeroed(&self, layout: Layout) -> *mut u8 {
        init();
        let mem = self.alloc(layout);
        memset(mem as *mut c_void, 0, layout.size()) as *mut u8
    }

    /// Reallocates the memory at ptr. This function is exactly like the system function. If there's
    #[inline]
    unsafe fn realloc(&self, ptr: *mut u8, layout: Layout, size: usize) -> *mut u8 {
        // We assume we are inited here because a realloc should never be called if we haven't allocated before.
        #[cfg(feature = "debug")]
        libc_println!("Reallocate -> Previous Size: {}, Size: {},  Ptr: {:?}", layout.size(), size, ptr);
        let layout_size = layout.size();
        if ptr.is_null() {
            return self.alloc(Layout::from_size_align(size, layout.align()).unwrap());
        }
        if size == 0 {
            self.dealloc(ptr, layout);
        }
        let header = get_block_header(ptr);
        if layout_size != header.size {
            libc_println!("Data at {:?} had size {}, expected {}", ptr, layout_size, header.size);
            exit(1);
        }
        if size < layout_size {
            // If the size of the block is not large enough to fit another header, allocate a new block instead.
            if layout_size - size < 26 {
                return self.realloc_new(ptr, layout, size);
            }
            split_block(ptr, size);
            return ptr;
        }
        // If we don't have another block after this, allocate a new block and free this block.
        if header.next_block.is_null() {
            return self.realloc_new(ptr, layout, size);
        }
        let next_header = get_block_header(header.next_block);
        // If we don't have enough memory in the next block or it isn't free, allocate a new block and free this block.
        if !next_header.free || next_header.size + BLOCK_HEADER_SIZE + header.size < size || (next_header.size + BLOCK_HEADER_SIZE + header.size) - size < 26 {
            return self.realloc_new(ptr, layout, size);
        }
        // For the case where there is enough room in the next block for the new size, combine the two blocks together and then split them again if there's room left over.
        //print_state();
        combine_blocks(ptr);
        //print_state();
        let header = get_block_header(ptr);
        if header.size > size {
            split_block(ptr, size);
        }
        #[cfg(feature = "verbose")]
            print_state();
        ptr
    }
}