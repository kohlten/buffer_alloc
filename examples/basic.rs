use std::alloc::{GlobalAlloc, Layout};
use std::time::{SystemTime, UNIX_EPOCH};
use libc::{c_void, free, malloc};
use buffer_alloc::BufferAlloc;
use buffer_alloc::util::{print_state};

#[global_allocator]
static BUFFER_ALLOCATOR: BufferAlloc = BufferAlloc;

fn get_time() -> u64 {
    SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_nanos() as u64
}

fn main() {
    let mut pointers = Vec::new();
    let mut times = Vec::new();
    for i in 0..10 {
        let layout = Layout::from_size_align((2.0 as f32).powi(i) as usize, 1).unwrap();
        for _ in 0..1000 {
            let layout_clone = layout.clone();
            let start = get_time();
            let ptr = unsafe { BufferAlloc.alloc(layout_clone) };
            let time = get_time() - start;
            times.push(time);
            println!("Time: {}, Size: {}", time, layout.size());
            pointers.push((ptr, layout.clone()));
        }
    }
    for value in pointers {
        unsafe { BufferAlloc.dealloc(value.0, value.1) }
    }
    let mut min = times[0];
    let mut max = times[0];
    let mut average = 0;
    for value in times.iter() {
        if *value < min {
            min = *value;
        }
        if *value > max {
            max = *value;
        }
        average += *value;
    }
    average /= times.len() as u64;
    println!("Min Time: {}, Max Time: {}, Average Time: {}, Allocations: {}", min, max, average, times.len());
    let mut pointers = Vec::new();
    let mut times = Vec::new();
    for i in 0..10 {
        let layout = Layout::from_size_align((2.0 as f32).powi(i) as usize, 1).unwrap();
        for _ in 0..1000 {
            let layout_clone = layout.clone();
            let start = get_time();
            let ptr = unsafe { malloc(layout.size()) as *mut u8 };
            times.push(get_time() - start);
            pointers.push((ptr, layout.clone()));
        }
    }
    for value in pointers {
        unsafe { free(value.0 as *mut c_void) }
    }
    let mut min = times[0];
    let mut max = times[0];
    let mut average = 0;
    for value in times.iter() {
        if *value < min {
            min = *value;
        }
        if *value > max {
            max = *value;
        }
        average += *value;
    }
    average /= times.len() as u64;
    println!("Min Time: {}, Max Time: {}, Average Time: {}, Allocations: {}", min, max, average, times.len());
    let mut array = Vec::new();
    for _ in 0..1000 {
        array.push(42);
    }
    for i in 0..array.len() {
        print!("{}, ", array[i]);
    }
    println!();
    print_state();
}